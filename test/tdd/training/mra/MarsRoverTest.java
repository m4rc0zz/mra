package tdd.training.mra;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {
	MarsRover rover;
	
	@Before
	public void setUp() {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		rover = new MarsRover(10, 10, planetObstacles);
	}

	@Test
	public void test() throws MarsRoverException {		
		assertTrue(rover.planetContainsObstacleAt(2, 3));
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	// test user story 2
	public void testCommand() throws MarsRoverException {		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	// test user story 3 for turning right 
	public void testCommandRight() throws MarsRoverException {		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	// test user story 3 for turning left
	public void testCommandLeft() throws MarsRoverException {		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	// test user story 4 for movement forward
	public void testCommandForward() throws MarsRoverException {
		rover.setPosition(7, 6, 'N');
		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	
	@Test
	// test user story 4 for movement forward
	public void testCommandForward2() throws MarsRoverException {
		rover.setPosition(7, 6, 'S');
		assertEquals("(7,5,S)", rover.executeCommand("f"));
	}
	
	@Test
	// test user story 5 for movement backward
	public void testCommandBackward() throws MarsRoverException {
		rover.setPosition(5, 8, 'E');
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	// test user story 6 for combined movement
	public void testCommandCombined() throws MarsRoverException {
		rover.setPosition(0, 0, 'N');
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	// test user story 7 for wrapping
	public void testWrapping() throws MarsRoverException {
		assertEquals("(0,8,N)", rover.executeCommand("bb"));
		
		rover.setPosition(0, 8, 'N');
		assertEquals("(0,1,N)", rover.executeCommand("fff"));
	}
	
	@Test
	// test user story 8 single obstacle
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}

	@Test
	// test user story 9 multiple obstacles
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	// test user story 10 wrapping and obstacles
	public void testWrappingObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	// test user story 10 wrapping and obstacles
	public void testWrappingObstaclesRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(9,0)");
		rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)(9,0)", rover.executeCommand("rb"));
	}
	
	
	
	@Test
	// test user story 11 tour around the planet
	public void testTour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		rover = new MarsRover(6, 6, planetObstacles);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
	
}
