package tdd.training.mra;

import java.util.List;

public class MarsRover {
	protected int planetX;
	protected int planetY;
	protected List<String> planetObstacles;
	protected int roverX;
	protected int roverY;
	protected char dir;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.roverX = 0;
		this.roverY = 0;
		this.dir = 'N';
	}

	private String getPosition() {
		return "("+roverX+","+roverY+","+dir+")";
	}

	public void setPosition(int roverX, int roverY, char dir) {
		if(roverY<0) this.roverY = (planetY-1);
		else if(roverY>(planetY-1)) this.roverY = 0;
		else this.roverY = roverY;

		if(roverX<0) this.roverX = (planetX-1);
		else if(roverX>(planetX-1)) this.roverX = 0;
		else this.roverX = roverX;

		this.dir = dir;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		return this.planetObstacles.contains("("+x+","+y+")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		int incX;
		int incY;
		String obstacle = "";
		if( commandString.equals("") ) return getPosition();

		char[] chars = commandString.toCharArray();
		for(char c : chars) {
			incX=0; incY=0;
			// right movement 
			if( c=='r' ) {
				switch(this.dir) {
				case 'N': this.dir='E'; break;
				case 'E': this.dir='S'; break;
				case 'S': this.dir='W'; break;
				case 'W': this.dir='N'; break;
				default: throw new MarsRoverException("errore sulla posizione");
				}
			}
			// left movement 
			else if( c=='l' ) {
				switch(this.dir) {
				case 'N': this.dir='W'; break;
				case 'E': this.dir='N'; break;
				case 'S': this.dir='E'; break;
				case 'W': this.dir='S'; break;
				default: throw new MarsRoverException("errore sulla posizione");
				}
			}
			// forward movement 
			else if( c=='f') {
				switch(this.dir) {
				case 'N': incY=incY+1; break;
				case 'E': incX=incX+1; break;
				case 'S': incY=incY-1; break;
				case 'W': incX=incX-1; break;
				default: throw new MarsRoverException("errore sulla posizione");
				}
			}
			// backward movement 
			else if( c=='b') {
				switch(this.dir) {
				case 'N': incY=incY-1; break;
				case 'E': incX=incX-1; break;
				case 'S': incY=incY+1; break;
				case 'W': incX=incX+1; break;
				default: throw new MarsRoverException("errore sulla posizione");
				}
			}		
			
			int x=val(roverX+incX);
			int y=val(roverY+incY);
			
			if(planetContainsObstacleAt(x, y)) {

				if(!obstacle.contains("("+x+","+y+")")) obstacle += "("+x+","+y+")";

			} else {
				roverX += incX;
				roverY += incY;
				setPosition(roverX, roverY, dir);
			}
		}
		return getPosition()+obstacle;
	}

	public int val(int i) {
		if(i>(planetX-1)) i=0;
		else if(i<0) i=(planetX-1);
		return i;
	}

}
